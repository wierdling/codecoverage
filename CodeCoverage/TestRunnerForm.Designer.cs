﻿namespace CodeCoverage
{
    partial class TestRunnerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TestRunnerForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadTestToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.saveTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._dllsDataGridView = new System.Windows.Forms.DataGridView();
            this._dllNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._includeDllColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._deleteDllColumn = new System.Windows.Forms.DataGridViewButtonColumn();
            this._addDllButton = new System.Windows.Forms.Button();
            this._includesDataGridView = new System.Windows.Forms.DataGridView();
            this._includeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._includeIncludeColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._deleteFilterColumn = new System.Windows.Forms.DataGridViewButtonColumn();
            this._addIncludeButton = new System.Windows.Forms.Button();
            this._runButton = new System.Windows.Forms.Button();
            this._addFilterbutton = new System.Windows.Forms.Button();
            this._filterDataGridView = new System.Windows.Forms.DataGridView();
            this._filterColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._includeFilterColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewButtonColumn1 = new System.Windows.Forms.DataGridViewButtonColumn();
            this._filterContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._clearAllButton = new System.Windows.Forms.Button();
            this._addFilterHelpButton = new System.Windows.Forms.Button();
            this._addIncludeHelpButton = new System.Windows.Forms.Button();
            this._addDllHelpButton = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._dllsDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._includesDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._filterDataGridView)).BeginInit();
            this._filterContextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem,
            this.loadTestToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(724, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.settingsToolStripMenuItem.Text = "Settings";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // loadTestToolStripMenuItem
            // 
            this.loadTestToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadTestToolStripMenuItem1,
            this.saveTestToolStripMenuItem});
            this.loadTestToolStripMenuItem.Name = "loadTestToolStripMenuItem";
            this.loadTestToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.loadTestToolStripMenuItem.Text = "Coverages";
            // 
            // loadTestToolStripMenuItem1
            // 
            this.loadTestToolStripMenuItem1.Name = "loadTestToolStripMenuItem1";
            this.loadTestToolStripMenuItem1.Size = new System.Drawing.Size(148, 22);
            this.loadTestToolStripMenuItem1.Text = "Load Coverage";
            this.loadTestToolStripMenuItem1.Click += new System.EventHandler(this.loadTestToolStripMenuItem1_Click);
            // 
            // saveTestToolStripMenuItem
            // 
            this.saveTestToolStripMenuItem.Name = "saveTestToolStripMenuItem";
            this.saveTestToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.saveTestToolStripMenuItem.Text = "Save Coverage";
            this.saveTestToolStripMenuItem.Click += new System.EventHandler(this.saveTestToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.helpToolStripMenuItem.Text = "Help";
            this.helpToolStripMenuItem.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
            // 
            // _dllsDataGridView
            // 
            this._dllsDataGridView.AllowUserToAddRows = false;
            this._dllsDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._dllsDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._dllsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._dllsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._dllNameColumn,
            this._includeDllColumn,
            this._deleteDllColumn});
            this._dllsDataGridView.Location = new System.Drawing.Point(12, 27);
            this._dllsDataGridView.Name = "_dllsDataGridView";
            this._dllsDataGridView.RowHeadersVisible = false;
            this._dllsDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._dllsDataGridView.Size = new System.Drawing.Size(700, 135);
            this._dllsDataGridView.TabIndex = 0;
            this._dllsDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this._DataGridView_CellContentClick);
            // 
            // _dllNameColumn
            // 
            this._dllNameColumn.HeaderText = "Dll";
            this._dllNameColumn.Name = "_dllNameColumn";
            this._dllNameColumn.ReadOnly = true;
            this._dllNameColumn.Width = 44;
            // 
            // _includeDllColumn
            // 
            this._includeDllColumn.FalseValue = "false";
            this._includeDllColumn.HeaderText = "Include";
            this._includeDllColumn.Name = "_includeDllColumn";
            this._includeDllColumn.TrueValue = "true";
            this._includeDllColumn.Width = 48;
            // 
            // _deleteDllColumn
            // 
            this._deleteDllColumn.HeaderText = "Delete";
            this._deleteDllColumn.Name = "_deleteDllColumn";
            this._deleteDllColumn.Width = 44;
            // 
            // _addDllButton
            // 
            this._addDllButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._addDllButton.Location = new System.Drawing.Point(249, 168);
            this._addDllButton.Name = "_addDllButton";
            this._addDllButton.Size = new System.Drawing.Size(75, 23);
            this._addDllButton.TabIndex = 1;
            this._addDllButton.Text = "Add &Dll";
            this._addDllButton.UseVisualStyleBackColor = true;
            this._addDllButton.Click += new System.EventHandler(this._addDllButton_Click);
            // 
            // _includesDataGridView
            // 
            this._includesDataGridView.AllowUserToAddRows = false;
            this._includesDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this._includesDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._includesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._includesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._includeColumn,
            this._includeIncludeColumn,
            this._deleteFilterColumn});
            this._includesDataGridView.Location = new System.Drawing.Point(12, 206);
            this._includesDataGridView.Name = "_includesDataGridView";
            this._includesDataGridView.RowHeadersVisible = false;
            this._includesDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._includesDataGridView.Size = new System.Drawing.Size(250, 169);
            this._includesDataGridView.TabIndex = 2;
            this._includesDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this._DataGridView_CellContentClick);
            // 
            // _includeColumn
            // 
            this._includeColumn.HeaderText = "Include Category";
            this._includeColumn.Name = "_includeColumn";
            this._includeColumn.Width = 103;
            // 
            // _includeIncludeColumn
            // 
            this._includeIncludeColumn.FalseValue = "false";
            this._includeIncludeColumn.HeaderText = "Inclue";
            this._includeIncludeColumn.Name = "_includeIncludeColumn";
            this._includeIncludeColumn.TrueValue = "true";
            this._includeIncludeColumn.Width = 42;
            // 
            // _deleteFilterColumn
            // 
            this._deleteFilterColumn.HeaderText = "Delete";
            this._deleteFilterColumn.Name = "_deleteFilterColumn";
            this._deleteFilterColumn.Width = 44;
            // 
            // _addIncludeButton
            // 
            this._addIncludeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._addIncludeButton.Location = new System.Drawing.Point(79, 381);
            this._addIncludeButton.Name = "_addIncludeButton";
            this._addIncludeButton.Size = new System.Drawing.Size(75, 23);
            this._addIncludeButton.TabIndex = 3;
            this._addIncludeButton.Text = "Add &Include";
            this._addIncludeButton.UseVisualStyleBackColor = true;
            this._addIncludeButton.Click += new System.EventHandler(this._addIncludeButton_Click);
            // 
            // _runButton
            // 
            this._runButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._runButton.Location = new System.Drawing.Point(637, 412);
            this._runButton.Name = "_runButton";
            this._runButton.Size = new System.Drawing.Size(75, 23);
            this._runButton.TabIndex = 7;
            this._runButton.Text = "&Run Tests";
            this._runButton.UseVisualStyleBackColor = true;
            this._runButton.Click += new System.EventHandler(this._runButton_Click);
            // 
            // _addFilterbutton
            // 
            this._addFilterbutton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._addFilterbutton.Location = new System.Drawing.Point(492, 381);
            this._addFilterbutton.Name = "_addFilterbutton";
            this._addFilterbutton.Size = new System.Drawing.Size(75, 23);
            this._addFilterbutton.TabIndex = 5;
            this._addFilterbutton.Text = "Add &Filter";
            this._addFilterbutton.UseVisualStyleBackColor = true;
            this._addFilterbutton.Click += new System.EventHandler(this._addFilterbutton_Click);
            // 
            // _filterDataGridView
            // 
            this._filterDataGridView.AllowUserToAddRows = false;
            this._filterDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._filterDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._filterDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._filterDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._filterColumn,
            this._includeFilterColumn,
            this.dataGridViewButtonColumn1});
            this._filterDataGridView.ContextMenuStrip = this._filterContextMenuStrip;
            this._filterDataGridView.Location = new System.Drawing.Point(292, 206);
            this._filterDataGridView.Name = "_filterDataGridView";
            this._filterDataGridView.RowHeadersVisible = false;
            this._filterDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._filterDataGridView.Size = new System.Drawing.Size(420, 169);
            this._filterDataGridView.TabIndex = 4;
            this._filterDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this._DataGridView_CellContentClick);
            this._filterDataGridView.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this._filterDataGridView_CellMouseDown);
            // 
            // _filterColumn
            // 
            this._filterColumn.HeaderText = "Filter";
            this._filterColumn.Name = "_filterColumn";
            this._filterColumn.Width = 54;
            // 
            // _includeFilterColumn
            // 
            this._includeFilterColumn.FalseValue = "false";
            this._includeFilterColumn.HeaderText = "Include";
            this._includeFilterColumn.Name = "_includeFilterColumn";
            this._includeFilterColumn.TrueValue = "true";
            this._includeFilterColumn.Width = 48;
            // 
            // dataGridViewButtonColumn1
            // 
            this.dataGridViewButtonColumn1.HeaderText = "Delete";
            this.dataGridViewButtonColumn1.Name = "dataGridViewButtonColumn1";
            this.dataGridViewButtonColumn1.Width = 44;
            // 
            // _filterContextMenuStrip
            // 
            this._filterContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyToolStripMenuItem});
            this._filterContextMenuStrip.Name = "_filterContextMenuStrip";
            this._filterContextMenuStrip.Size = new System.Drawing.Size(100, 26);
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(99, 22);
            this.copyToolStripMenuItem.Text = "Copy";
            this.copyToolStripMenuItem.Click += new System.EventHandler(this.copyToolStripMenuItem_Click);
            // 
            // _clearAllButton
            // 
            this._clearAllButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._clearAllButton.Location = new System.Drawing.Point(12, 412);
            this._clearAllButton.Name = "_clearAllButton";
            this._clearAllButton.Size = new System.Drawing.Size(75, 23);
            this._clearAllButton.TabIndex = 8;
            this._clearAllButton.Text = "Clear All";
            this._clearAllButton.UseVisualStyleBackColor = true;
            this._clearAllButton.Click += new System.EventHandler(this._clearAllButton_Click);
            // 
            // _addFilterHelpButton
            // 
            this._addFilterHelpButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._addFilterHelpButton.BackColor = System.Drawing.Color.Transparent;
            this._addFilterHelpButton.BackgroundImage = global::CodeCoverage.Properties.Resources.help;
            this._addFilterHelpButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this._addFilterHelpButton.FlatAppearance.BorderSize = 0;
            this._addFilterHelpButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._addFilterHelpButton.Location = new System.Drawing.Point(573, 381);
            this._addFilterHelpButton.Name = "_addFilterHelpButton";
            this._addFilterHelpButton.Size = new System.Drawing.Size(23, 23);
            this._addFilterHelpButton.TabIndex = 11;
            this._addFilterHelpButton.UseVisualStyleBackColor = false;
            this._addFilterHelpButton.Click += new System.EventHandler(this._addFilterHelpButton_Click);
            // 
            // _addIncludeHelpButton
            // 
            this._addIncludeHelpButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._addIncludeHelpButton.BackColor = System.Drawing.Color.Transparent;
            this._addIncludeHelpButton.BackgroundImage = global::CodeCoverage.Properties.Resources.help;
            this._addIncludeHelpButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this._addIncludeHelpButton.FlatAppearance.BorderSize = 0;
            this._addIncludeHelpButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._addIncludeHelpButton.Location = new System.Drawing.Point(160, 381);
            this._addIncludeHelpButton.Name = "_addIncludeHelpButton";
            this._addIncludeHelpButton.Size = new System.Drawing.Size(23, 23);
            this._addIncludeHelpButton.TabIndex = 10;
            this._addIncludeHelpButton.UseVisualStyleBackColor = false;
            this._addIncludeHelpButton.Click += new System.EventHandler(this._addIncludeHelpButton_Click);
            // 
            // _addDllHelpButton
            // 
            this._addDllHelpButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._addDllHelpButton.BackColor = System.Drawing.Color.Transparent;
            this._addDllHelpButton.BackgroundImage = global::CodeCoverage.Properties.Resources.help;
            this._addDllHelpButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this._addDllHelpButton.FlatAppearance.BorderSize = 0;
            this._addDllHelpButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._addDllHelpButton.Location = new System.Drawing.Point(330, 168);
            this._addDllHelpButton.Name = "_addDllHelpButton";
            this._addDllHelpButton.Size = new System.Drawing.Size(23, 23);
            this._addDllHelpButton.TabIndex = 9;
            this._addDllHelpButton.UseVisualStyleBackColor = false;
            this._addDllHelpButton.Click += new System.EventHandler(this._addDllHelpButton_Click);
            // 
            // TestRunnerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(724, 447);
            this.Controls.Add(this._addFilterHelpButton);
            this.Controls.Add(this._addIncludeHelpButton);
            this.Controls.Add(this._addDllHelpButton);
            this.Controls.Add(this._clearAllButton);
            this.Controls.Add(this._addFilterbutton);
            this.Controls.Add(this._filterDataGridView);
            this.Controls.Add(this._runButton);
            this.Controls.Add(this._addIncludeButton);
            this.Controls.Add(this._includesDataGridView);
            this.Controls.Add(this._addDllButton);
            this.Controls.Add(this._dllsDataGridView);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "TestRunnerForm";
            this.Text = "Code Coverage Test Runner";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TestRunnerForm_FormClosing);
            this.Shown += new System.EventHandler(this.Form1_Shown);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._dllsDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._includesDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._filterDataGridView)).EndInit();
            this._filterContextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.DataGridView _dllsDataGridView;
        private System.Windows.Forms.Button _addDllButton;
        private System.Windows.Forms.DataGridView _includesDataGridView;
        private System.Windows.Forms.Button _addIncludeButton;
        private System.Windows.Forms.Button _runButton;
        private System.Windows.Forms.Button _addFilterbutton;
        private System.Windows.Forms.DataGridView _filterDataGridView;
        private System.Windows.Forms.ToolStripMenuItem loadTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadTestToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem saveTestToolStripMenuItem;
        private System.Windows.Forms.Button _clearAllButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dllNameColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _includeDllColumn;
        private System.Windows.Forms.DataGridViewButtonColumn _deleteDllColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _includeColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _includeIncludeColumn;
        private System.Windows.Forms.DataGridViewButtonColumn _deleteFilterColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _filterColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _includeFilterColumn;
        private System.Windows.Forms.DataGridViewButtonColumn dataGridViewButtonColumn1;
        private System.Windows.Forms.ContextMenuStrip _filterContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.Button _addDllHelpButton;
        private System.Windows.Forms.Button _addIncludeHelpButton;
        private System.Windows.Forms.Button _addFilterHelpButton;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
    }
}

