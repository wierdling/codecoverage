﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeCoverage
{
    public static class Container
    {
        private static readonly Dictionary<string, Object> _presenters = new Dictionary<string, object>();

        public static void AddPresenter(string key, Object value)
        {
            if (_presenters.ContainsKey(key))
            {
                _presenters.Remove(key);
            }
            _presenters.Add(key, value);
        }

        public static Object GetPresenter(string key)
        {
            if (_presenters.ContainsKey(key))
            {
                return _presenters[key];
            }
            return null;
        }
    }
}
