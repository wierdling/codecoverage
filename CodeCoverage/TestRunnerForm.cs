﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using CodeCoverage.Entities;
using CodeCoverage.EventArguments;
using CodeCoverage.Presenters;

namespace CodeCoverage
{
    public partial class TestRunnerForm : Form, ITestRunnerView
    {
        public event EventHandler SettingsClicked;
        public event EventHandler AddDllClicked;
        public event EventHandler AddIncludeClicked;
        public event EventHandler AddFilterClicked;
        public event EventHandler RunClicked;
        public event EventHandler HelpClicked;
        public event EventHandler SaveRunnerClicked;
        public event EventHandler LoadRunnerClicked;

        public event EventHandler<GenericEventArgument<string>> AddOutput;
        public event EventHandler<GenericEventArgument<string>> AddHtmlOutput;  

        public TestRunnerForm()
        {
            InitializeComponent();
        }

        public void AddDll(string dll)
        {
            _dllsDataGridView.Rows.Add(dll, true, "Delete");
        }

        public void AddInclude(string include)
        {
            _includesDataGridView.Rows.Add(include, true, "Delete");
        }

        public void AddFilter(string filter)
        {
            _filterDataGridView.Rows.Add(filter, true, "Delete");
        }

        public string Dlls
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                foreach (DataGridViewRow row in _dllsDataGridView.Rows)
                {
                    if (DBNull.Value.Equals(row.Cells[0].Value) || null == row.Cells[0].Value ||
                        string.IsNullOrEmpty(row.Cells[0].Value.ToString().Trim())) continue;
                    if (row.Cells["_includeDllColumn"].Value.Equals(true))
                    {
                        sb.AppendFormat("{0} ", row.Cells[0].Value);
                    }
                }
                return sb.ToString();
            }
        }

        public string Includes
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                foreach (DataGridViewRow row in _includesDataGridView.Rows)
                {
                    if (DBNull.Value.Equals(row.Cells[0].Value) ||
                        string.IsNullOrEmpty(row.Cells[0].Value?.ToString().Trim())) continue;
                    if (row.Cells[_includeIncludeColumn.Name].Value.Equals(true))
                    {
                        sb.AppendFormat("/include:{0} ", row.Cells[0].Value);
                    }
                }
                return sb.ToString();
            }
        }

        public string Filters
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                foreach (DataGridViewRow row in _filterDataGridView.Rows)
                {
                    if (DBNull.Value.Equals(row.Cells[0].Value) || null == row.Cells[0].Value ||
                        string.IsNullOrEmpty(row.Cells[0].Value.ToString().Trim())) continue;
                    if (row.Cells[_includeFilterColumn.Name].Value.Equals(true))
                    {
                        sb.AppendFormat("{0} ", row.Cells[0].Value);
                    }
                }
                return sb.ToString();
            }
        }

        public RunnerEntity RunnerEntity
        {
            get
            {
                RunnerEntity testEntity = new RunnerEntity();

                foreach (DataGridViewRow row in _dllsDataGridView.Rows)
                {
                    if (DBNull.Value.Equals(row.Cells[0].Value) ||
                        string.IsNullOrEmpty(row.Cells[0].Value?.ToString().Trim())) continue;
                    testEntity.Dlls.Add(row.Cells[0].Value.ToString());
                }

                foreach (DataGridViewRow row in _includesDataGridView.Rows)
                {
                    if (DBNull.Value.Equals(row.Cells[0].Value) ||
                        string.IsNullOrEmpty(row.Cells[0].Value?.ToString().Trim())) continue;
                    testEntity.Includes.Add(row.Cells[0].Value.ToString());
                }

                foreach (DataGridViewRow row in _filterDataGridView.Rows)
                {
                    if (DBNull.Value.Equals(row.Cells[0].Value) ||
                        string.IsNullOrEmpty(row.Cells[0].Value?.ToString().Trim())) continue;
                    testEntity.Filters.Add(row.Cells[0].Value.ToString());
                }
                return testEntity;
            }
        }

        public void ClearGrids()
        {
            _dllsDataGridView.Rows.Clear();
            _includesDataGridView.Rows.Clear();
            _filterDataGridView.Rows.Clear();
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SettingsClicked?.Invoke(sender, e);
        }

        private void _addDllButton_Click(object sender, EventArgs e)
        {
            AddDllClicked?.Invoke(sender, e);
        }

        private void _DataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView) sender;
            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0 &&
                ((DataGridViewButtonColumn)senderGrid.Columns[e.ColumnIndex]).HeaderText.Equals("delete", StringComparison.OrdinalIgnoreCase))
            {
                senderGrid.Rows.RemoveAt(e.RowIndex);
            }
        }

        private void _addIncludeButton_Click(object sender, EventArgs e)
        {
            AddIncludeClicked?.Invoke(sender, e);
        }

        private void _addFilterbutton_Click(object sender, EventArgs e)
        {
            AddFilterClicked?.Invoke(sender, e);
        }

        private void _runButton_Click(object sender, EventArgs e)
        {
            List<string> errors;
            if (!Validate(out errors))
            {
                ShowErrors(errors);
                return;
            }

            RunClicked?.Invoke(sender, e);
        }

        private void saveTestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<string> errors;
            if (!Validate(out errors))
            {
                ShowErrors(errors);
                return;
            }

            SaveRunnerClicked?.Invoke(sender, e);
        }

        private void ShowErrors(List<string> errors)
        {
            MessageBox.Show(string.Format("There were the following validation errors:{0}{1}",
                Environment.NewLine, string.Join(Environment.NewLine, errors)), "Validation Errors");
        }

        private bool Validate(out List<string> errors)
        {
            errors = new List<string>();
            bool isValid = true;
            if (string.IsNullOrEmpty(Properties.Settings.Default.NUnitPath))
            {
                isValid = false;
                errors.Add("No path to NUnit");
            }

            if (string.IsNullOrEmpty(Properties.Settings.Default.OpenCoverPath))
            {
                isValid = false;
                errors.Add("No path to OpenCover");
            }

            if (string.IsNullOrEmpty(Properties.Settings.Default.ReportGeneratorPath))
            {
                isValid = false;
                errors.Add("No path to ReportGenerator");
            }

            if (_dllsDataGridView.Rows.Count == 0)
            {
                isValid = false;
                errors.Add("Please select at least one dll to test.");
            }

            return isValid;
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            _dllsDataGridView.Rows.Clear();
            _includesDataGridView.Rows.Clear();
            _filterDataGridView.Rows.Clear();
        }

        private void loadTestToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            LoadRunnerClicked?.Invoke(sender, e);
        }

        private void _clearAllButton_Click(object sender, EventArgs e)
        {
            ClearGrids();
        }

        private void TestRunnerForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.TestRunnerFormLocation = Location;
            Properties.Settings.Default.TestRunnerFormSize = Size;
            Properties.Settings.Default.TestRunnerFormMaximized = WindowState == FormWindowState.Maximized;
            Properties.Settings.Default.Save();
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int selectedRowIndex = _filterDataGridView.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            if (selectedRowIndex < 0) return;
            Clipboard.SetText(_filterDataGridView.Rows[selectedRowIndex].Cells[0].Value.ToString());
        }

        private void _filterDataGridView_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button.Equals(MouseButtons.Right))
            {
                _filterDataGridView.ClearSelection();
                int selectedRow = e.RowIndex;
                if (selectedRow > 0)
                {
                    _filterDataGridView.Rows[selectedRow].Selected = true;
                }
            }
        }

        private void _addDllHelpButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Browse to the location of the test assemblies (where the tests are, not what they test).",
                "Add Dll Help", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void _addIncludeHelpButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show(
                "Add what categories you want to include (from the [Category(xxx)] attribute on your test classes/tests).",
                "Add Include Help", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void _addFilterHelpButton_Click(object sender, EventArgs e)
        {
            string message = @"Filters are used to include/exclude classes from the code coverage results.
An add filter is formatted:
+[<dll name>]<class name>
An exclude filter is formatted:
-[<dll name>]<class name> to exclude.";
            MessageBox.Show(message, "Add Filter Help", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HelpClicked?.Invoke(sender, e);
        }
    }
}
