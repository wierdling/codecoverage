﻿using System;

namespace CodeCoverage.EventArguments
{
    public class GenericEventArgument<T> : EventArgs
    {
        public T Data { get; set; }

        public GenericEventArgument(T data)
        {
            Data = data;
        } 
    }
}
