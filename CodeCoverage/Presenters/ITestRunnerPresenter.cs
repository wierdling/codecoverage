﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeCoverage.Entities;

namespace CodeCoverage.Presenters
{
    public interface ITestRunnerPresenter
    {
        void AddDllToView(string filename);
        void AddFilterToView(string filter);
        void AddIncludeToView(string include);
        void ClearViewGrids();
    }
}
