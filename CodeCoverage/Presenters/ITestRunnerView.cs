﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CodeCoverage.Entities;
using CodeCoverage.EventArguments;

namespace CodeCoverage.Presenters
{
    public interface ITestRunnerView
    {
        event EventHandler SettingsClicked;
        event EventHandler AddDllClicked;
        event EventHandler AddIncludeClicked;
        event EventHandler AddFilterClicked;
        event EventHandler RunClicked;
        event EventHandler HelpClicked;
        event EventHandler SaveRunnerClicked;
        event EventHandler LoadRunnerClicked;

        event EventHandler<GenericEventArgument<string>> AddOutput;
        event EventHandler<GenericEventArgument<string>> AddHtmlOutput;

        void AddDll(string dll);

        void AddFilter(string filter);

        void AddInclude(string include);

        FormStartPosition StartPosition { get; set; }
        FormWindowState WindowState { get; set; }
        Point Location { set; }
        Size Size { set; }

        string Dlls { get; }

        string Filters { get; }

        string Includes { get; }

        void ClearGrids();

        RunnerEntity RunnerEntity { get; }
    }
}
