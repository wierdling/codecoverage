﻿using System;
using CodeCoverage.Controllers;
using CodeCoverage.Entities;

namespace CodeCoverage.Presenters
{
    public class TestRunnerPresenter : ITestRunnerPresenter
    {
        public ITestRunnerView View { get; set; }

        private readonly ITestRunnerController _controller;

        public TestRunnerPresenter(ITestRunnerView view)
        {
            View = view;
            _controller = new TestRunnerController(this);

            View.AddDllClicked += View_AddDllClicked;
            View.AddIncludeClicked += View_AddIncludeClicked;
            View.AddFilterClicked += View_AddFilter;
            View.SettingsClicked += View_SettingsClicked;
            View.HelpClicked += View_HelpClicked;
            View.SaveRunnerClicked += View_SaveRunnerClicked;
            View.LoadRunnerClicked += View_LoadRunnerClicked;

            View.RunClicked += View_RunClicked;
        }

        public void ClearViewGrids()
        {
            View.ClearGrids();
        }

        private void View_LoadRunnerClicked(object sender, EventArgs e)
        {
            _controller.LoadRunner();
        }

        private void View_SaveRunnerClicked(object sender, EventArgs e)
        {
            _controller.SaveRunner(View.RunnerEntity);
        }

        private void View_HelpClicked(object sender, EventArgs e)
        {
            _controller.ShowHelp();
        }

        private void View_RunClicked(object sender, EventArgs e)
        {
            _controller.RunCoverage(new RunCoverageEntity(View.Dlls, View.Includes, View.Filters));
        }

        public void AddFilterToView(string filter)
        {
            View.AddFilter(filter);
        }

        public void AddIncludeToView(string include)
        {
            View.AddInclude(include);
        }

        public void AddDllToView(string filename)
        {
            View.AddDll(filename);
        }

        private void View_AddIncludeClicked(object sender, EventArgs e)
        {
            _controller.AddInclude();
        }

        private void View_SettingsClicked(object sender, EventArgs e)
        {
            _controller.ShowSettings();
        }

        private void View_AddDllClicked(object sender, EventArgs e)
        {
            _controller.LoadDll();
        }

        private void View_AddFilter(object sender, EventArgs e)
        {
            _controller.AddFilter();
        }
    }
}
