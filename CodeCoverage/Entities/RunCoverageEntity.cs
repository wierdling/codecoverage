﻿namespace CodeCoverage.Entities
{
    public class RunCoverageEntity
    {
        public string Dlls { get; set; }
        public string Includes { get; set; }
        public string Filters { get; set; }

        public RunCoverageEntity(string dlls, string includes, string filters)
        {
            Dlls = dlls;
            Includes = includes;
            Filters = filters;
        }
    }
}
