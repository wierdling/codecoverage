﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeCoverage.Entities
{
    [Serializable]
    public class RunnerEntity
    {
        public string OpenCoverPath { get; set; }
        public string ReportGeneratorPath { get; set; }
        public string NUnitPath { get; set; }

        public List<string> Dlls { get; set; }
        public List<string> Includes { get; set; }
        public List<String> Filters { get; set; }

        public RunnerEntity()
        {
            Dlls = new List<string>();
            Includes = new List<string>();
            Filters = new List<string>();
        }
    }
}
