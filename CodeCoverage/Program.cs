﻿using System;
using System.Windows.Forms;
using CodeCoverage.Presenters;

namespace CodeCoverage
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            ITestRunnerView form = new TestRunnerForm();
            form.StartPosition = FormStartPosition.Manual;
            if (Properties.Settings.Default.TestRunnerFormMaximized)
            {
                form.WindowState = FormWindowState.Maximized;
            }
            else
            {
                form.Location = Properties.Settings.Default.TestRunnerFormLocation;
                form.Size = Properties.Settings.Default.TestRunnerFormSize;
            }
            TestRunnerPresenter presenter = new TestRunnerPresenter(form);
            Container.AddPresenter(Constants.PresenterNames.TestRunnerPresenter, presenter);
            Application.Run((TestRunnerForm)form);
        }
    }
}
