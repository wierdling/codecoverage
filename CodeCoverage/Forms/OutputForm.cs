﻿using System;
using System.Windows.Forms;

namespace CodeCoverage.Forms
{
    public partial class OutputForm : Form
    {
        public OutputForm(string output)
        {
            InitializeComponent();
            ShowOutput(output);
        }

        public void SetLabel(string text)
        {
            _statusLabel.Text = text;
        }

        public void AppendOutput(string text)
        {
            try
            {
                _outputRichTextBox.AppendText(string.Format("{0}{1}", text, Environment.NewLine));
            }
            catch
            {
                // ignore errors.
            }
        }

        public void SetOutput(string text)
        {
            _outputRichTextBox.Text = text;
        }

        private void ShowOutput(string output)
        {
            _outputRichTextBox.Text = output;
        }

        private void _exitButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void OutputForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.OutputFormLocation = Location;
            Properties.Settings.Default.OutputFormSize = Size;
            Properties.Settings.Default.OutputFormMaximized = WindowState == FormWindowState.Maximized;
            Properties.Settings.Default.Save();
        }
    }
}
