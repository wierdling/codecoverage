﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace CodeCoverage.Forms
{
    public partial class HelpForm : Form
    {
        public HelpForm()
        {
            InitializeComponent();
            _openCoverLinkLabel.Links.Add(0, 9, "https://github.com/opencover/opencover/releases");
            _nunitLinkLabel.Links.Add(0, 4, "http://nunit.org/?p=download");
            _reportGeneratorLinkLabel.Links.Add(0, 16, "https://github.com/danielpalme/ReportGenerator/releases");
        }

        private void _openCoverLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(_openCoverLinkLabel.Links[0].LinkData.ToString());
        }

        private void _nunitLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(_nunitLinkLabel.Links[0].LinkData.ToString());
        }

        private void _reportGeneratorLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(_reportGeneratorLinkLabel.Links[0].LinkData.ToString());
        }

        private void _exitButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void HelpForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.HelpFormLocation = Location;
            Properties.Settings.Default.Save();
        }
    }
}
