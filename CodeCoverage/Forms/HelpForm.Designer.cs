﻿namespace CodeCoverage.Forms
{
    partial class HelpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HelpForm));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._openCoverLinkLabel = new System.Windows.Forms.LinkLabel();
            this._nunitLinkLabel = new System.Windows.Forms.LinkLabel();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this._reportGeneratorLinkLabel = new System.Windows.Forms.LinkLabel();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this._exitButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Open Cover";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(439, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "OpenCover can be downloaded from Github.  This program was tested with OpenCover " +
    "4.6.";
            // 
            // _openCoverLinkLabel
            // 
            this._openCoverLinkLabel.AutoSize = true;
            this._openCoverLinkLabel.Location = new System.Drawing.Point(20, 67);
            this._openCoverLinkLabel.Name = "_openCoverLinkLabel";
            this._openCoverLinkLabel.Size = new System.Drawing.Size(61, 13);
            this._openCoverLinkLabel.TabIndex = 0;
            this._openCoverLinkLabel.TabStop = true;
            this._openCoverLinkLabel.Text = "OpenCover";
            this._openCoverLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this._openCoverLinkLabel_LinkClicked);
            // 
            // _nunitLinkLabel
            // 
            this._nunitLinkLabel.AutoSize = true;
            this._nunitLinkLabel.Location = new System.Drawing.Point(20, 156);
            this._nunitLinkLabel.Name = "_nunitLinkLabel";
            this._nunitLinkLabel.Size = new System.Drawing.Size(34, 13);
            this._nunitLinkLabel.TabIndex = 1;
            this._nunitLinkLabel.TabStop = true;
            this._nunitLinkLabel.Text = "NUnit";
            this._nunitLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this._nunitLinkLabel_LinkClicked);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 129);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(401, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "NUnit Console is part of the NUnit install.  This program was tested with NUnit 2" +
    ".6.4.";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 98);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(124, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "NUnit Console";
            // 
            // _reportGeneratorLinkLabel
            // 
            this._reportGeneratorLinkLabel.AutoSize = true;
            this._reportGeneratorLinkLabel.Location = new System.Drawing.Point(20, 246);
            this._reportGeneratorLinkLabel.Name = "_reportGeneratorLinkLabel";
            this._reportGeneratorLinkLabel.Size = new System.Drawing.Size(89, 13);
            this._reportGeneratorLinkLabel.TabIndex = 2;
            this._reportGeneratorLinkLabel.TabStop = true;
            this._reportGeneratorLinkLabel.Text = "Report Generator";
            this._reportGeneratorLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this._reportGeneratorLinkLabel_LinkClicked);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 219);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(413, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Report Generator is what creates the final html file.  It can be downloaded from " +
    "Github.";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 188);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(151, 20);
            this.label6.TabIndex = 6;
            this.label6.Text = "Report Generator";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 280);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(201, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "This program was written by James Long.";
            // 
            // _exitButton
            // 
            this._exitButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._exitButton.Location = new System.Drawing.Point(409, 266);
            this._exitButton.Name = "_exitButton";
            this._exitButton.Size = new System.Drawing.Size(53, 27);
            this._exitButton.TabIndex = 3;
            this._exitButton.Text = "&Exit";
            this._exitButton.UseVisualStyleBackColor = true;
            this._exitButton.Click += new System.EventHandler(this._exitButton_Click);
            // 
            // HelpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(474, 302);
            this.Controls.Add(this._exitButton);
            this.Controls.Add(this.label7);
            this.Controls.Add(this._reportGeneratorLinkLabel);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this._nunitLinkLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this._openCoverLinkLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(482, 329);
            this.MinimumSize = new System.Drawing.Size(482, 329);
            this.Name = "HelpForm";
            this.Text = "Help";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.HelpForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.LinkLabel _openCoverLinkLabel;
        private System.Windows.Forms.LinkLabel _nunitLinkLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.LinkLabel _reportGeneratorLinkLabel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button _exitButton;
    }
}