﻿using System;
using System.Windows.Forms;

namespace CodeCoverage.Forms
{
    public partial class SettingsForm : Form
    {
        public SettingsForm()
        {
            InitializeComponent();
            SetValuesFromSettingsFile();
        }

        private void SetValuesFromSettingsFile()
        {
            _openCoverPathTextBox.Text = Properties.Settings.Default.OpenCoverPath;
            _reportGeneratorPathTextBox.Text = Properties.Settings.Default.ReportGeneratorPath;
            _nunitExePathTextBox.Text = Properties.Settings.Default.NUnitPath;
        }

        private void _saveButton_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(_openCoverPathTextBox.Text.Trim()))
            {
                Properties.Settings.Default.OpenCoverPath = _openCoverPathTextBox.Text;
            }
            if (!string.IsNullOrEmpty(_reportGeneratorPathTextBox.Text.Trim()))
            {
                Properties.Settings.Default.ReportGeneratorPath = _reportGeneratorPathTextBox.Text;
            }
            if (!string.IsNullOrEmpty(_nunitExePathTextBox.Text.Trim()))
            {
                Properties.Settings.Default.NUnitPath = _nunitExePathTextBox.Text;
            }
            Properties.Settings.Default.Save();
            DialogResult = DialogResult.OK;
        }

        private void _exitButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void _setOpenCoverPathButton_Click(object sender, EventArgs e)
        {
            SetPath("Open");
        }

        private void _setReportGeneratorPathButton_Click(object sender, EventArgs e)
        {
            SetPath("Rgen");
        }

        private void _setNunitExePathButton_Click(object sender, EventArgs e)
        {
            SetPath("NUnit");
        }

        private void SetPath(string type)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "executables (*.exe)|*.exe|All Files (*.*)|*.*";
            ofd.Multiselect = false;
            string title = "Nunit Console";
            if (type.Equals("Open"))
            {
                title = "OpenCover";
            }
            else if (type.Equals("Rgen"))
            {
                title = "ReportGenerator";
            }
            ofd.Title = string.Format("Open {0}", title);
            ;
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                switch (type)
                {
                    case "Open":
                        _openCoverPathTextBox.Text = ofd.FileName;
                        break;
                    case "Rgen":
                        _reportGeneratorPathTextBox.Text = ofd.FileName;
                        break;
                    default:
                        _nunitExePathTextBox.Text = ofd.FileName;
                        break;
                }
            }
        }

        private void SettingsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.SettingsFormLocation = Location;
            Properties.Settings.Default.Save();
        }
    }
}
