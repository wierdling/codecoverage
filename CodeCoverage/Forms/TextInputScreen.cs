﻿using System;
using System.Windows.Forms;
using CodeCoverage.EventArguments;

namespace CodeCoverage.Forms
{
    public partial class TextInputScreen : Form
    {
        public event EventHandler<GenericEventArgument<string>> OnTextAccepted;
        public TextInputScreen(string title)
        {
            InitializeComponent();
            SetTitle(title);
        }

        private void SetTitle(string title)
        {
            Text = title;
        }

        public void SetText(string text)
        {
            _inputRichTextbox.Text = text;
        }

        private void _addButton_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(_inputRichTextbox.Text))
            {
                OnTextAccepted?.Invoke(this, new GenericEventArgument<string>(_inputRichTextbox.Text));
            }
            _inputRichTextbox.Text = "";
            _inputRichTextbox.Select(0, 1);
        }

        private void _cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(_inputRichTextbox.Text))
            {
                OnTextAccepted?.Invoke(this, new GenericEventArgument<string>(_inputRichTextbox.Text));
            }
            DialogResult = DialogResult.OK;
        }

        private void TextInputScreen_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.TextInputFormLocation = Location;
            Properties.Settings.Default.Save();
        }
    }
}
