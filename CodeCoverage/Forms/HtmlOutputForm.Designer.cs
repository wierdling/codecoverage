﻿namespace CodeCoverage.Forms
{
    partial class HtmlOutputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HtmlOutputForm));
            this._webControl = new System.Windows.Forms.WebBrowser();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this._backButton = new System.Windows.Forms.ToolStripButton();
            this._forwardButton = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _webControl
            // 
            this._webControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._webControl.Location = new System.Drawing.Point(3, 28);
            this._webControl.MinimumSize = new System.Drawing.Size(20, 20);
            this._webControl.Name = "_webControl";
            this._webControl.ScriptErrorsSuppressed = true;
            this._webControl.Size = new System.Drawing.Size(567, 403);
            this._webControl.TabIndex = 0;
            this._webControl.Navigating += new System.Windows.Forms.WebBrowserNavigatingEventHandler(this._webControl_Navigating);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._backButton,
            this._forwardButton});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(570, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // _backButton
            // 
            this._backButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._backButton.Image = global::CodeCoverage.Properties.Resources._112_LeftArrowLong_Green_32x42_72;
            this._backButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._backButton.Name = "_backButton";
            this._backButton.Size = new System.Drawing.Size(23, 22);
            this._backButton.Text = "Back";
            this._backButton.Click += new System.EventHandler(this._backButton_Click);
            // 
            // _forwardButton
            // 
            this._forwardButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._forwardButton.Image = global::CodeCoverage.Properties.Resources._112_RightArrowLong_Green_32x42_72;
            this._forwardButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._forwardButton.Name = "_forwardButton";
            this._forwardButton.Size = new System.Drawing.Size(23, 22);
            this._forwardButton.Text = "Forward";
            this._forwardButton.Click += new System.EventHandler(this._forwardButton_Click);
            // 
            // HtmlOutputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(570, 431);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this._webControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "HtmlOutputForm";
            this.Text = "Coverage Output";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.HtmlOutputForm_FormClosing);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.WebBrowser _webControl;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton _backButton;
        private System.Windows.Forms.ToolStripButton _forwardButton;
    }
}