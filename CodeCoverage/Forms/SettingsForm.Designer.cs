﻿namespace CodeCoverage.Forms
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.label1 = new System.Windows.Forms.Label();
            this._openCoverPathTextBox = new System.Windows.Forms.TextBox();
            this._setOpenCoverPathButton = new System.Windows.Forms.Button();
            this._setReportGeneratorPathButton = new System.Windows.Forms.Button();
            this._reportGeneratorPathTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this._saveButton = new System.Windows.Forms.Button();
            this._exitButton = new System.Windows.Forms.Button();
            this._setNunitExePathButton = new System.Windows.Forms.Button();
            this._nunitExePathTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(38, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Open Cover Exe:";
            // 
            // _openCoverPathTextBox
            // 
            this._openCoverPathTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._openCoverPathTextBox.Location = new System.Drawing.Point(132, 23);
            this._openCoverPathTextBox.Name = "_openCoverPathTextBox";
            this._openCoverPathTextBox.Size = new System.Drawing.Size(380, 20);
            this._openCoverPathTextBox.TabIndex = 1;
            // 
            // _setOpenCoverPathButton
            // 
            this._setOpenCoverPathButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._setOpenCoverPathButton.Location = new System.Drawing.Point(518, 21);
            this._setOpenCoverPathButton.Name = "_setOpenCoverPathButton";
            this._setOpenCoverPathButton.Size = new System.Drawing.Size(31, 23);
            this._setOpenCoverPathButton.TabIndex = 2;
            this._setOpenCoverPathButton.Text = "Set";
            this._setOpenCoverPathButton.UseVisualStyleBackColor = true;
            this._setOpenCoverPathButton.Click += new System.EventHandler(this._setOpenCoverPathButton_Click);
            // 
            // _setReportGeneratorPathButton
            // 
            this._setReportGeneratorPathButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._setReportGeneratorPathButton.Location = new System.Drawing.Point(518, 47);
            this._setReportGeneratorPathButton.Name = "_setReportGeneratorPathButton";
            this._setReportGeneratorPathButton.Size = new System.Drawing.Size(31, 23);
            this._setReportGeneratorPathButton.TabIndex = 5;
            this._setReportGeneratorPathButton.Text = "Set";
            this._setReportGeneratorPathButton.UseVisualStyleBackColor = true;
            this._setReportGeneratorPathButton.Click += new System.EventHandler(this._setReportGeneratorPathButton_Click);
            // 
            // _reportGeneratorPathTextBox
            // 
            this._reportGeneratorPathTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._reportGeneratorPathTextBox.Location = new System.Drawing.Point(132, 49);
            this._reportGeneratorPathTextBox.Name = "_reportGeneratorPathTextBox";
            this._reportGeneratorPathTextBox.Size = new System.Drawing.Size(380, 20);
            this._reportGeneratorPathTextBox.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Report Generator Exe:";
            // 
            // _saveButton
            // 
            this._saveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._saveButton.Location = new System.Drawing.Point(12, 127);
            this._saveButton.Name = "_saveButton";
            this._saveButton.Size = new System.Drawing.Size(75, 23);
            this._saveButton.TabIndex = 6;
            this._saveButton.Text = "&Save";
            this._saveButton.UseVisualStyleBackColor = true;
            this._saveButton.Click += new System.EventHandler(this._saveButton_Click);
            // 
            // _exitButton
            // 
            this._exitButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._exitButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._exitButton.Location = new System.Drawing.Point(485, 127);
            this._exitButton.Name = "_exitButton";
            this._exitButton.Size = new System.Drawing.Size(75, 23);
            this._exitButton.TabIndex = 7;
            this._exitButton.Text = "&Exit";
            this._exitButton.UseVisualStyleBackColor = true;
            this._exitButton.Click += new System.EventHandler(this._exitButton_Click);
            // 
            // _setNunitExePathButton
            // 
            this._setNunitExePathButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._setNunitExePathButton.Location = new System.Drawing.Point(518, 73);
            this._setNunitExePathButton.Name = "_setNunitExePathButton";
            this._setNunitExePathButton.Size = new System.Drawing.Size(31, 23);
            this._setNunitExePathButton.TabIndex = 10;
            this._setNunitExePathButton.Text = "Set";
            this._setNunitExePathButton.UseVisualStyleBackColor = true;
            this._setNunitExePathButton.Click += new System.EventHandler(this._setNunitExePathButton_Click);
            // 
            // _nunitExePathTextBox
            // 
            this._nunitExePathTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._nunitExePathTextBox.Location = new System.Drawing.Point(132, 75);
            this._nunitExePathTextBox.Name = "_nunitExePathTextBox";
            this._nunitExePathTextBox.Size = new System.Drawing.Size(380, 20);
            this._nunitExePathTextBox.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "NUnit Console Exe:";
            // 
            // SettingsForm
            // 
            this.AcceptButton = this._saveButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._exitButton;
            this.ClientSize = new System.Drawing.Size(572, 162);
            this.Controls.Add(this._setNunitExePathButton);
            this.Controls.Add(this._nunitExePathTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this._exitButton);
            this.Controls.Add(this._saveButton);
            this.Controls.Add(this._setReportGeneratorPathButton);
            this.Controls.Add(this._reportGeneratorPathTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this._setOpenCoverPathButton);
            this.Controls.Add(this._openCoverPathTextBox);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SettingsForm";
            this.Text = "SettingsForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SettingsForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox _openCoverPathTextBox;
        private System.Windows.Forms.Button _setOpenCoverPathButton;
        private System.Windows.Forms.Button _setReportGeneratorPathButton;
        private System.Windows.Forms.TextBox _reportGeneratorPathTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button _saveButton;
        private System.Windows.Forms.Button _exitButton;
        private System.Windows.Forms.Button _setNunitExePathButton;
        private System.Windows.Forms.TextBox _nunitExePathTextBox;
        private System.Windows.Forms.Label label3;
    }
}