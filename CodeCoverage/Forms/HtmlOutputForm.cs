﻿using System;
using System.Windows.Forms;

namespace CodeCoverage.Forms
{
    public partial class HtmlOutputForm : Form
    {
        public HtmlOutputForm()
        {
            InitializeComponent();
        }

        public void SetFile(string fileName)
        {
            if (IsDisposed) return;
            _webControl.Url = new Uri(string.Format("file://{0}", fileName));
            _backButton.Enabled = false;
            _forwardButton.Enabled = false;
        }

        private void _backButton_Click(object sender, EventArgs e)
        {
            if (_webControl.CanGoBack)
            {
                _webControl.GoBack();
            }
            _backButton.Enabled = _webControl.CanGoBack;
            _forwardButton.Enabled = _webControl.CanGoForward;
           
        }

        private void _forwardButton_Click(object sender, EventArgs e)
        {
            if (_webControl.CanGoForward)
            {
                _webControl.GoForward();
            }
            _forwardButton.Enabled = _webControl.CanGoForward;
            _backButton.Enabled = _webControl.CanGoBack;
        }

        private void _webControl_Navigating(object sender, WebBrowserNavigatingEventArgs e)
        {
            _backButton.Enabled = true;
        }

        private void HtmlOutputForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.CoverageOutputFormLocation = Location;
            Properties.Settings.Default.CoverageOutputFormSize = Size;
            Properties.Settings.Default.CoverageOutputFormMaximized = WindowState == FormWindowState.Maximized;
            Properties.Settings.Default.Save();
        }
    }
}
