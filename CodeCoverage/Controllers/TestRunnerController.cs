﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using CodeCoverage.Entities;
using CodeCoverage.Forms;
using CodeCoverage.Presenters;
using CodeCoverage.Utilities;
using ThreadState = System.Threading.ThreadState;

namespace CodeCoverage.Controllers
{
    public class TestRunnerController : ITestRunnerController
    {
        private readonly ITestRunnerPresenter _presenter;
        private OutputForm _output;
        private HtmlOutputForm _htmlForm;
        private Thread _runThread;

        public TestRunnerController(ITestRunnerPresenter presenter)
        {
            _presenter = presenter;
        }

        public void ShowSettings()
        {
            SettingsForm settingsForm = new SettingsForm
            {
                StartPosition = FormStartPosition.Manual,
                Location = Properties.Settings.Default.SettingsFormLocation
            };
            settingsForm.ShowDialog();
        }

        public void LoadDll()
        {
            OpenFileDialog ofd = new OpenFileDialog
            {
                Multiselect = true,
                Filter = @"dll files (*.dll)|*.dll|All files (*.*)|*.*"
            };
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                foreach (string fileName in ofd.FileNames)
                {
                    _presenter.AddDllToView(fileName);
                }
            }
        }

        public void AddInclude()
        {
            TextInputScreen input = new TextInputScreen("Enter Unit Test Category")
            {
                StartPosition = FormStartPosition.Manual,
                Location = Properties.Settings.Default.TextInputFormLocation
            };
            input.OnTextAccepted += (o, argument) => _presenter.AddIncludeToView(argument.Data);

            input.ShowDialog();
        }

        public void AddFilter()
        {
            TextInputScreen input = new TextInputScreen("Enter Filter")
            {
                StartPosition = FormStartPosition.Manual,
                Location = Properties.Settings.Default.TextInputFormLocation
            };
            input.OnTextAccepted += (o, argument) => _presenter.AddFilterToView(argument.Data);
            input.SetText("+[<dll name>]<class name>");
            input.ShowDialog();
        }

        public void RunCoverage(RunCoverageEntity entity)
        {
            OpenOutput();
            OpenHtmlOutput();

            if (null != _runThread && _runThread.IsAlive && _runThread.ThreadState == ThreadState.Running)
            {
                _runThread.Interrupt();
            }
            _runThread = new Thread(Run);
            _runThread.Start(entity);
        }

        public void ShowHelp()
        {
            HelpForm hf = new HelpForm
            {
                StartPosition = FormStartPosition.Manual,
                Location = Properties.Settings.Default.HelpFormLocation
            };
            hf.ShowDialog();
        }

        public void SaveRunner(RunnerEntity entity)
        {
            entity.NUnitPath = Properties.Settings.Default.NUnitPath;
            entity.OpenCoverPath = Properties.Settings.Default.OpenCoverPath;
            entity.ReportGeneratorPath = Properties.Settings.Default.ReportGeneratorPath;

            string xml = XmlSerializationHelper.ToXml(entity);

            SaveFileDialog sfd = new SaveFileDialog
            {
                InitialDirectory = GetApplicationDataDirectory(),
                Filter = @"xml files (*.xml)|*.xml|All Files (*.*)|*.*"
            };

            if (sfd.ShowDialog().Equals(DialogResult.OK))
            {
                string fileName = sfd.FileName;
                if (!fileName.ToLower().EndsWith(".xml"))
                {
                    fileName += ".xml";
                }
                string fullName = Path.Combine(GetApplicationDataDirectory(), fileName);
                using (TextWriter tWriter = new StreamWriter(fullName))
                {
                    tWriter.Write(xml);
                }
            }
        }

        public void LoadRunner()
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Multiselect = false;
            ofd.Filter = "xml files (*.xml)|*.xml|All Files (*.*)|*.*";
            ofd.InitialDirectory = GetApplicationDataDirectory();
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    using (TextReader tReader = new StreamReader(ofd.FileName))
                    {
                        string xml = tReader.ReadToEnd();
                        RunnerEntity entity = XmlSerializationHelper.ToObject<RunnerEntity>(xml);
                        if (null != entity)
                        {
                            _presenter.ClearViewGrids();

                            foreach (string dll in entity.Dlls)
                            {
                                _presenter.AddDllToView(dll);
                            }

                            foreach (string include in entity.Includes)
                            {
                                _presenter.AddIncludeToView(include);
                            }

                            foreach (string filter in entity.Filters)
                            {
                                _presenter.AddFilterToView(filter);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(
                        string.Format("There was an error when trying to load the given test.  Error: {0}", ex.Message),
                        "Load Error");
                }
            }
        }

        #region Private Methods

        public static string StartupPath => Application.StartupPath;

        private string GetOutputDirectory()
        {
            return Path.Combine(StartupPath, "ReportOutput");
        }

        private string GetOutputReportFile()
        {
            return Path.Combine(GetOutputDirectory(), "TestReport.xml");
        }

        private string GetHtmlOutputDirectory(bool clean)
        {
            string directory = Path.Combine(GetOutputDirectory(), "HtmlOutput");
            if (Directory.Exists(directory) && clean)
            {
                DirectoryInfo dInfo = new DirectoryInfo(directory);
                foreach (FileInfo fileInfo in dInfo.GetFiles())
                {
                    fileInfo.Delete();
                }
                List<string> directories = dInfo.GetDirectories().Select(childInfo => childInfo.FullName).ToList();
                foreach (string d in directories)
                {
                    Directory.Delete(d, true);
                }
            }
            else
            {
                Directory.CreateDirectory(directory);
            }
            return directory;
        }

        private string GetApplicationDataDirectory()
        {
            string dir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "CodeCoverage");
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            return dir;
        }

        private void Run(object runEntity)
        {
            try
            {
                RunCoverageEntity entity = (RunCoverageEntity) runEntity;
                UpdateOutputText(
                    $"Testing starting at {DateTime.Now.ToShortDateString()} {DateTime.Now.ToShortTimeString()}");
                string outputReportFile = GetOutputReportFile();
                if (File.Exists(outputReportFile))
                {
                    File.Delete(outputReportFile);
                }

                string htmlDirectory = GetHtmlOutputDirectory(false);
                string zipFile = Path.Combine(GetOutputDirectory(),
                    string.Format("TestRunner_{0}_{1}.zip", DateTime.Now.ToShortDateString().Replace("/", ""),
                        DateTime.Now.ToShortTimeString().Replace(":", "_")));
                DirectoryCompressor.Compress(htmlDirectory, zipFile);

                StringBuilder sb = new StringBuilder();
                sb.Append("-register:user ");
                sb.AppendFormat("-target:\"{0}\" ", Properties.Settings.Default.NUnitPath);
                sb.AppendFormat(" -targetargs:\"{0} /noshadow {1} \" ", entity.Dlls, entity.Includes);
                sb.AppendFormat(" -filter:\"{0}\" ", entity.Filters);
                sb.AppendFormat(" -mergebyhash -skipautoprops -output:\"{0}\" ", outputReportFile);
                Process p = new Process
                {
                    StartInfo =
                    {
                        FileName = Properties.Settings.Default.OpenCoverPath,
                        Arguments = sb.ToString(),
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        RedirectStandardError = true,
                        CreateNoWindow = true
                    }
                };
                p.OutputDataReceived += OutputHandler;
                p.ErrorDataReceived += ErrorHandler;
                int exitCode;
                try
                {
                    p.Start();
                    p.BeginErrorReadLine();
                    p.BeginOutputReadLine();
                    do
                    {
                        if (p.HasExited) continue;
                        p.Refresh();
                    } while (!p.WaitForExit(10000));
                    exitCode = p.ExitCode;
                    UpdateOutputText(string.Format("OpenCover has finished with an exit code of {0}", exitCode));
                }
                finally
                {
                    p.Close();
                }

                if (exitCode == 0) // run the report generator
                {
                    sb = new StringBuilder();
                    sb.AppendFormat("-reports:\"{0}\" ", outputReportFile);
                    sb.AppendFormat("-targetdir:\"{0}\" ", GetHtmlOutputDirectory(true));
                    p = new Process
                    {
                        StartInfo =
                    {
                        FileName = Properties.Settings.Default.ReportGeneratorPath,
                        Arguments = sb.ToString(),
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        RedirectStandardError = true,
                        CreateNoWindow = true
                    }
                    };
                    p.OutputDataReceived += OutputHandler;
                    p.ErrorDataReceived += ErrorHandler;
                    try
                    {
                        p.Start();
                        p.BeginErrorReadLine();
                        p.BeginOutputReadLine();
                        do
                        {
                            if (!p.HasExited)
                            {
                                p.Refresh();
                            }
                        } while (!p.WaitForExit(10000));
                        exitCode = p.ExitCode;
                        UpdateOutputText(string.Format("Report Generator has finished with an exit code of {0}", exitCode));
                        if (exitCode == 0)
                        {
                            string path = Path.Combine(GetHtmlOutputDirectory(false), "index.htm");
                            SetHtmlFile(path);
                            //Process.Start(path);
                        }
                    }
                    finally
                    {
                        p.Close();
                    }
                }
            }
            finally
            {
                MessageBox.Show("Finished running test.", "Finished", MessageBoxButtons.OK);
                UpdateOutputText(
    $"Testing finished at {DateTime.Now.ToShortDateString()} {DateTime.Now.ToShortTimeString()}");
            }
        }

        private delegate void UpdateOutputTextDelegate(string text);

        private void UpdateOutputText(string text)
        {
            if (null == _output || _output.IsDisposed)
            {
                OpenOutput();
            }
            if (null == _output) return;

            if (_output.InvokeRequired)
            {
                UpdateOutputTextDelegate del = UpdateOutputText;
                _output.Invoke(del, text);
                return;
            }

            _output.AppendOutput(text);
        }

        private delegate void SetHtmlFileDelegate(string fileName);

        private void SetHtmlFile(string fileName)
        {
            if (null == _htmlForm || _htmlForm.IsDisposed)
            {
                OpenHtmlOutput();
            }

            if (null == _htmlForm) return;

            if (_htmlForm.InvokeRequired)
            {
                SetHtmlFileDelegate del = SetHtmlFile;
                _htmlForm.Invoke(del, fileName);
                return;
            }

            _htmlForm.SetFile(fileName);
        }

        private void OutputHandler(object sendingProcess, DataReceivedEventArgs outline)
        {
            UpdateOutputText(outline.Data);
        }

        private void ErrorHandler(object sendingProcess, DataReceivedEventArgs outline)
        {
            if (string.IsNullOrEmpty(outline.Data)) return;

            UpdateOutputText(string.Format("ERROR ERROR ERROR{0}{1}{0}ERROR ERROR ERROR{0}",
                Environment.NewLine, outline.Data));
        }

        private void OpenOutput()
        {
            _output = new OutputForm("") { StartPosition = FormStartPosition.Manual };
            if (Properties.Settings.Default.OutputFormMaximized)
            {
                _output.WindowState = FormWindowState.Maximized;
            }
            else
            {
                _output.Location = Properties.Settings.Default.OutputFormLocation;
                _output.Size = Properties.Settings.Default.OutputFormSize;
            }
            _output.Show();
        }

        private void OpenHtmlOutput()
        {
            _htmlForm = new HtmlOutputForm { StartPosition = FormStartPosition.Manual };
            if (Properties.Settings.Default.CoverageOutputFormMaximized)
            {
                _htmlForm.WindowState = FormWindowState.Maximized;
            }
            else
            {
                _htmlForm.Location = Properties.Settings.Default.CoverageOutputFormLocation;
                _htmlForm.Size = Properties.Settings.Default.CoverageOutputFormSize;
            }
            _htmlForm.Show();
        }

        #endregion Private Methods
    }
}
