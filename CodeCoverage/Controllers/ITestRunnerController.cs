﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeCoverage.Entities;

namespace CodeCoverage.Controllers
{
    public interface ITestRunnerController
    {
        void LoadDll();
        void AddInclude();
        void AddFilter();
        void ShowSettings();

        void RunCoverage(RunCoverageEntity entity);

        void ShowHelp();

        void SaveRunner(RunnerEntity entity);

        void LoadRunner();
    }
}
