﻿using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace CodeCoverage.Utilities
{
    public static class XmlSerializationHelper
    {
        public static T ToObject<T>(string xml)
        {
            XmlSerializer xs = new XmlSerializer(typeof (T));
            using (StringReader sr = new StringReader(xml))
            {
                using (XmlTextReader reader = new XmlTextReader(sr))

                    return (T)xs.Deserialize(reader);
            }
        }

        public static string ToXml<T>(T obj)
        {
            XmlSerializer xs = new XmlSerializer(typeof (T));
            StringBuilder sb = new StringBuilder();
            using (StringWriter sw = new StringWriter(sb))
            {
                xs.Serialize(sw, obj);
            }
            return sb.ToString();
        }
    }
}
