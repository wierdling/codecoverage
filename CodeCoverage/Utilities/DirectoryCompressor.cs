﻿using System;
using System.IO;
using System.IO.Compression;

namespace CodeCoverage.Utilities
{
    public static class DirectoryCompressor
    {
        public static void Compress(string sourceFolder, string finalName)
        {
            if (!Directory.Exists(sourceFolder)) return;
            DirectoryInfo dInfo = new DirectoryInfo(sourceFolder);
            if (dInfo.GetFiles().Length < 1) return;

            try
            {
                if (File.Exists(finalName))
                {
                    File.Delete(finalName);
                }
                ZipFile.CreateFromDirectory(sourceFolder, finalName);

                foreach (FileInfo fileInfo in dInfo.GetFiles())
                {
                    if (fileInfo.Name.EndsWith("zip", StringComparison.OrdinalIgnoreCase)) continue;
                    File.Delete(fileInfo.FullName);
                }
            }
            catch (Exception ex)
            {
                
            }
        }
    }
}
